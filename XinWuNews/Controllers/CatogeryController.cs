﻿using Biz;
using Biz.Constant;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XinWuNews.Models.Catogery;

namespace XinWuNews.Controllers
{
    public class CatogeryController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ActionResult Index(string categoryGuid = "", int pageIndex = 0, string menu = "")
        {
            var result = new CategoryRespModel();

            if (string.IsNullOrEmpty(menu))
            {
                ViewData["menu"] = "home";
            }
            else
            {
                ViewData["menu"] = menu;
            }

            result.DisplaySearchCategory = new List<string>();

            result.DisplaySearchCategory.Add(ConstantValue.SpecialLinks);

            try
            {
                result.SpecialLink = ConstantValue.SpecialLinks;

                result.CategoryGuid = categoryGuid;

                result.StartDate = string.Empty;

                result.EndDate = string.Empty;

                result.PageIndex = pageIndex;
                
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return View(result);
        }

        #region 后台渲染模式，弃用
        /// <summary>
        /// 按钮检索
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="categoryGuid"></param>
        /// <returns></returns>
        //public ActionResult Search(string keyword, string startTime, string endTime, string categoryGuid = "012001", int pageIndex = 0, string emplId = "", string deptId="", string menu = "")
        //{
        //    var result = new CategoryRespModel();

        //    if (string.IsNullOrEmpty(menu))
        //    {
        //        ViewData["menu"] = "home";
        //    }
        //    else
        //    {
        //        ViewData["menu"] = menu;
        //    }

        //    result.DisplaySearchCategory = new List<string>();
        //    result.DisplaySearchCategory.Add(ConstantValue.SpecialLinks);

        //    if (" - " == startTime)
        //    {
        //        startTime = string.Empty;
        //    }

        //    if (" - " == endTime)
        //    {
        //        endTime = string.Empty;
        //    }

        //    result.StartDate = startTime;

        //    result.EndDate = endTime;

        //    try
        //    {
        //        //var emplId = string.Empty;

        //        //if (!string.IsNullOrEmpty(emplId))
        //        //{
        //        //    var roleBiz = new RoleBiz();

        //        //    roleId = roleBiz.GetRoleId(emplId);
        //        //}

        //        var biz = new CatogeryBiz();

        //        result.CategoryGuid = categoryGuid;

        //        result.Keyword = keyword;

        //        if (!string.IsNullOrEmpty(endTime))
        //        {
        //            DateTime endDate = DateTime.Today;
        //            DateTime.TryParse(endTime, out endDate);
        //            endDate = endDate.AddDays(1);

        //            endTime = endDate.ToString("yyyy-MM-dd");
        //        }

        //        if (!string.IsNullOrEmpty(deptId))
        //        {
        //            //1,2,3 => '1','2','3' SQL IN 参数化
        //            deptId = "'" + deptId.Replace(",", "','") + "'";
        //        }

        //        result.TotalCount = biz.SearchTotalCount(keyword, categoryGuid, startTime, endTime, emplId, deptId);

        //        result.TotalPage = (result.TotalCount + 20) / 20;

        //        result.PageIndex = pageIndex;

        //        result.SpecialLink = ConstantValue.SpecialLinks;

        //        pageIndex = pageIndex == 0 ? 1 : pageIndex;

        //        result.CategoryList = biz.SearchCatogeryNews(keyword, categoryGuid, startTime, endTime, (pageIndex - 1) * 20, pageIndex * 20, 49, emplId, deptId);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex);
        //    }

        //    return PartialView(result);
        //}

        #endregion
    }
}