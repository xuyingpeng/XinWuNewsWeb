﻿using Biz;
using Biz.Constant;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XinWuNews.Models.Home;

namespace XinWuNews.Controllers
{
    public class HomeController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ActionResult Index()
        {
            var result = new HomeModel();

            ViewData["menu"] = "home";

            try
            {
                //HttpCookie cookie = Request.Cookies["APPKIZTICKET"];

                //var emplId = string.Empty;

                //if (cookie != null && cookie.Value.Length > 6)
                //{
                    //emplId = cookie.Value.Substring(0, 6);

                    //var roleBiz = new RoleBiz();

                    //roleId = roleBiz.GetRoleId(emplId);
                //}

                var biz = new InfomationBiz();

                //重要新闻
                result.ImportantNews = biz.GetHomeNews(ConstantValue.ImportantNews, 12, 27);

                //重要通知
                result.ImportantNotice = biz.GetHomeNews(ConstantValue.ImportantNotice, 10, 21);

                //新吴动态
                result.XinWuDynamic = biz.GetHomeNews(ConstantValue.XinWuDynamic, 10, 23);

                //社区动态
                result.CommunityDynamic = biz.GetHomeNews(ConstantValue.CommunityDynamic, 10, 23);

                //解放思想大讨论动态
                result.VisitDynamicActivities = biz.GetHomeNews(ConstantValue.VisitDynamicActivities, 10, 23);

                //优选信息
                result.OptimizingInformation = biz.GetHomeNews(ConstantValue.OptimizingInformation, 10, 20);

                //部门通知
                result.DepartmentNotice = biz.GetHomeNews(ConstantValue.DepartmentNotice, 10, 21);

                //重要文件
                result.ImportantDocuments = biz.GetHomeNews(ConstantValue.ImportantDocuments, 10, 49);

                //部门文件
                result.DepartmentDocuments = biz.GetHomeNews(ConstantValue.DepartmentDocuments, 10, 49);

                //公示公告
                result.Announcement = biz.GetHomeNews(ConstantValue.Announcement, 10, 49);

                //一周工作计划
                result.WeeklySchedule = biz.GetHomeNews(ConstantValue.WeeklySchedule, 10, 49);

                //解放思想大讨论
                result.EmancipatesDiscussion = biz.GetHomeNews(ConstantValue.EmancipatesDiscussion, 10, 49);

                //263在行动
                result.Action263 = biz.GetHomeNews(ConstantValue.Action263, 10, 49);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return View(result);
        }

    }
}