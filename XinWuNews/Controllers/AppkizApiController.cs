﻿using Appkiz.Library.Common;
using Appkiz.Library.Security;
using Appkiz.Library.Security.Authentication;
using Biz;
using Biz.Constant;
using Model;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using XinWuNews.Models.AppkizApi;
using XinWuNews.Models.Catogery;
using XinWuNews.Models.Contact;

namespace XinWuNews.Controllers
{
    public class AppkizApiController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        [Route("api/appkiz/GetSpecialUrlList/")]
        /// <summary>
        /// 获取专题链接URL
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult GetSpecialUrlList()
        {
            var result = new ApiResponseVo<SpecialUrlDto>();

            try
            {
                var biz = new CatogeryBiz();

                var dbResult = biz.GetSpecialUrl(ConstantValue.SpecialLinksNum);

                result.Data = dbResult;
                result.Count = dbResult.Count;
                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        [Route("api/appkiz/AdvancedSearch/")]
        /// <summary>
        /// 按钮检索
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="categoryGuid"></param>
        /// <returns></returns>
        public IHttpActionResult AdvancedSearch(CategorySearchModel model)
        {
            var result = new SearchPageResponseVo<AdvancedSearchDto>();

            try
            {
                var biz = new CatogeryBiz();

                if (string.IsNullOrEmpty(model.startTime) || " - " == model.startTime)
                {
                    model.startTime = string.Empty;
                }

                if (string.IsNullOrEmpty(model.endTime) || " - " == model.endTime)
                {
                    model.endTime = string.Empty;
                }

                if (string.IsNullOrEmpty(model.keyword))
                {
                    model.keyword = string.Empty;
                }

                if (string.IsNullOrEmpty(model.categoryGuid))
                {
                    model.categoryGuid = string.Empty;
                }

                if (string.IsNullOrEmpty(model.deptId))
                {
                    model.deptId = string.Empty;
                }

                if (!string.IsNullOrEmpty(model.endTime))
                {
                    DateTime endDate = DateTime.Today;
                    DateTime.TryParse(model.endTime, out endDate);
                    endDate = endDate.AddDays(1);

                    model.endTime = endDate.ToString("yyyy-MM-dd");
                }

                if (!string.IsNullOrEmpty(model.categoryGuid))
                {
                    //1,2,3 => '1','2','3' SQL IN 参数化
                    model.categoryGuid = "'" + model.categoryGuid.Replace(",", "','") + "'";
                }

                if (!string.IsNullOrEmpty(model.deptId))
                {
                    //1,2,3 => '1','2','3' SQL IN 参数化
                    model.deptId = "'" + model.deptId.Replace(",", "','") + "'";
                }

                result.TotalCount = biz.AdvancedSearchTotalCount(model.keyword, model.categoryGuid, model.startTime, model.endTime, model.emplId, model.deptId);

                result.TotalPage = (result.TotalCount + 20 - 1) / 20;

                result.PageIndex = model.pageIndex;

                //model.pageIndex = model.pageIndex == 0 ? 1 : model.pageIndex;

                result.Data = biz.AdvancedSearchCatogeryNews(model.keyword, model.categoryGuid, model.startTime, model.endTime, model.pageIndex * 20, (model.pageIndex + 1) * 20, 49, model.emplId, model.deptId);

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        [Route("api/appkiz/CategorySearch/")]
        /// <summary>
        /// 按钮检索
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="categoryGuid"></param>
        /// <returns></returns>
        public IHttpActionResult CategorySearch(CategorySearchModel model)
        {
            //string keyword, string startTime, string endTime, string categoryGuid = "012001", int pageIndex = 0, string emplId = "", string deptId = "", string menu = ""
            var result = new SearchPageResponseVo<CategoryNewsDto>();

            if (string.IsNullOrEmpty(model.startTime) || " - " == model.startTime)
            {
                model.startTime = string.Empty;
            }

            if (string.IsNullOrEmpty(model.endTime) || " - " == model.endTime)
            {
                model.endTime = string.Empty;
            }

            if (string.IsNullOrEmpty(model.keyword))
            {
                model.keyword = string.Empty;
            }

            if (string.IsNullOrEmpty(model.categoryGuid))
            {
                model.categoryGuid = string.Empty;
            }

            if (string.IsNullOrEmpty(model.deptId))
            {
                model.deptId = string.Empty;
            }

            try
            {
                var biz = new CatogeryBiz();

                if (!string.IsNullOrEmpty(model.endTime))
                {
                    DateTime endDate = DateTime.Today;
                    DateTime.TryParse(model.endTime, out endDate);
                    endDate = endDate.AddDays(1);

                    model.endTime = endDate.ToString("yyyy-MM-dd");
                }

                if (!string.IsNullOrEmpty(model.deptId))
                {
                    //1,2,3 => '1','2','3' SQL IN 参数化
                    model.deptId = "'" + model.deptId.Replace(",", "','") + "'";
                }

                result.TotalCount = biz.SearchTotalCount(model.keyword, model.categoryGuid, model.startTime, model.endTime, model.emplId, model.deptId);

                result.TotalPage = (result.TotalCount + 20 -1) / 20;

                result.PageIndex = model.pageIndex;

                //model.pageIndex = model.pageIndex == 0 ? 1 : model.pageIndex;

                result.Data = biz.SearchCatogeryNews(model.keyword, model.categoryGuid, model.startTime, model.endTime, model.pageIndex * 20, (model.pageIndex +1) * 20, 43, model.emplId, model.deptId);

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 检查新闻浏览权限
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/GetHomeLink/")]
        public IHttpActionResult GetHomeLink()
        {
            var result = new ApiResponseVo<KeyValueModel>();

            try
            {
                var model1 = new KeyValueModel();

                model1.Key = "个人办公";
                model1.Value = ConstantValue.PrivateOfficeUrl;

                var datas = new List<KeyValueModel> ();
                datas.Add(model1);

                var biz = new CatogeryBiz();

                var model2 = new KeyValueModel();

                model2.Key = "督查工作";
                model2.Value = biz.GetCategoryOutUrl(model2.Key);

                datas.Add(model2);

                result.Data = datas;

                result.Succeed = true;

            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 检查新闻浏览权限
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/CheckUserLogin/")]
        public IHttpActionResult CheckUserLogin()
        {
            var result = new ApiResponseVo<string>();

            try
            {
                var identity = (User.Identity as AppkizIdentity);

                if (identity.Employee != null)
                {
                    result.Succeed = true;
                }
                else
                {
                    result.Succeed = false;
                }
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 检查新闻浏览权限
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/CheckUserPermission/")]
        public IHttpActionResult CheckUserPermission(UserPermissionModel model)
        {
            var result = new ApiResponseVo<string>();

            try
            {
                var identity = (User.Identity as AppkizIdentity);

                var roleBiz = new RoleBiz();

                var roleIdList = new List<string>();

                if (identity != null)
                {
                    roleIdList = roleBiz.GetRoleIdList(identity.Employee.EmplID);
                }

                var categoryBiz = new CatogeryBiz();

                var categoryPermission = categoryBiz.GetCategoryPermission(model.InfoId);

                if (string.IsNullOrEmpty(categoryPermission) || categoryPermission == "ALL" || roleIdList.Count == 0 || roleIdList.Exists(x => categoryPermission.Contains(x)))
                {
                    var infoBiz = new InfomationBiz();

                    result.Succeed = infoBiz.GetNewsPermission(model.InfoId, identity.Employee.EmplID);
                }
                else
                {
                    result.Succeed = false;
                }
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 查询所有部门
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/GetSliderList/")]
        public IHttpActionResult GetSliderList()
        {
            var result = string.Empty;

            try
            {
                result = GetSliderListAppkiz();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 查询高级搜索页面的部门列表
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/GetSearchDeptList/")]
        public IHttpActionResult GetSearchDeptList()
        {
            var result = string.Empty;

            try
            {
                result = GetAllDeptListAppkiz(true);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 查询所有部门
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/GetAllDeptList/")]
        public IHttpActionResult GetAllDeptList()
        {
            var result = string.Empty;

            try
            {
                result = GetAllDeptListAppkiz(false);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 查询所有部门
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/GetAllChannelist/")]
        public IHttpActionResult GetAllChannelist()
        {
            var result = string.Empty;

            try
            {
                result = GetAllChannelistAppkiz();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 查询所有部门
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/GetMenulist/")]
        public IHttpActionResult GetMenulist()
        {
            var result = string.Empty;

            try
            {
                result = GetMenuistAppkiz();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/login_site/")]
        public IHttpActionResult LoginSite(LoginSiteModel model)
        {
            var result = string.Empty;

            try
            {
                result = LoginSite(model.userName, model.userPwd);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }

        //
        [HttpPost]
        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/emplid_to_name/")]
        public IHttpActionResult EmplIdToName(EmplIdToNameModel model)
        {
            var result = new ApiResponseStrVo();

            try
            {
                var employee = (User.Identity as AppkizIdentity).Employee;

                result.Data = EmplIdToName(employee.EmplID);

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/get_contactInfos/")]
        public IHttpActionResult GetContactInfos(ContactInfoModel model)
        {
            var result = string.Empty;

            try
            {
                result = GetContactInfos(model.deptid, model.name, model.phone, model.tel);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        [Route("api/appkiz/get_departments/")]
        public IHttpActionResult GetDepartments(DepartmentModel model)
        {
            var result = string.Empty;

            try
            {
                result = GetDepartments(model.F_ID, model.first);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return Ok(result);
        }


        #region appkiz方法 

        /// <summary>
        /// 查询所有栏目
        /// </summary>
        /// <returns></returns>
        private static string GetMenuistAppkiz()
        {
            string datastr = "";

            try
            {
                using (SqlConnection con = new SqlConnection(ConstantValue.ConnectionString))
                {
                    con.Open();
                    string sql = @"SELECT [CategoryGuid]
                                          ,[CategoryNum]
                                          ,[CategoryName]
                                          ,[CategoryOutUrl]
                                      FROM [dbo].[WebDB_Category]
                                      WHERE Show=1
                                      ORDER BY OrderNum ASC ";

                    SqlCommand cmd = new SqlCommand(sql, con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    var dt = ds.Tables[0];

                    var categoryGuid = string.Empty;

                    var categoryNum = string.Empty;

                    var isLeaf = false;

                    var isParent = false;

                    var allLeaf = string.Empty;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        categoryGuid = dt.Rows[i]["CategoryGuid"].ToString();
                        categoryNum = dt.Rows[i]["CategoryNum"].ToString();

                        var name = dt.Rows[i]["CategoryName"].ToString();

                        isLeaf = false;
                        allLeaf = string.Empty;
                        isParent = true;

                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            var name1 = dt.Rows[j]["CategoryName"].ToString();

                            if (categoryNum != dt.Rows[j]["CategoryNum"].ToString()
                                && categoryNum.StartsWith(dt.Rows[j]["CategoryNum"].ToString()))
                            {
                                isParent = false;
                                break;
                            }

                            if (categoryNum != dt.Rows[j]["CategoryNum"].ToString()
                                && dt.Rows[j]["CategoryNum"].ToString().StartsWith(categoryNum))
                            {
                                allLeaf = allLeaf + ",{\"CategoryGuid\":\"" + dt.Rows[j]["CategoryGuid"].ToString() + "\",\"CategoryName\":\"" 
                                    + dt.Rows[j]["CategoryName"].ToString() + "\",\"CategoryOutUrl\":\"" + dt.Rows[j]["CategoryOutUrl"].ToString() 
                                    + "\",\"SubCategory\":" + "[]" + "}";
                                isLeaf = true;
                            }
                        }

                        if (isLeaf)
                        {
                            datastr = datastr + ",{\"CategoryGuid\":\"" + categoryGuid + "\",\"CategoryName\":\"" + dt.Rows[i]["CategoryName"].ToString() + "\",\"CategoryOutUrl\":\"" + dt.Rows[i]["CategoryOutUrl"].ToString() + "\",\"SubCategory\":[" + allLeaf .Substring(1)+ "]}";
                        }
                        else if(isParent)
                        {
                            datastr = datastr + ",{\"CategoryGuid\":\"" + categoryGuid + "\",\"CategoryName\":\"" + dt.Rows[i]["CategoryName"].ToString() + "\",\"CategoryOutUrl\":\"" + dt.Rows[i]["CategoryOutUrl"].ToString() + "\",\"SubCategory\":" + "[]" + "}";
                        }
                    }

                    //foreach (DataRow itemFather in ds.Tables[0].Rows)
                    //{
                    //    datastr += "{\"CategoryGuid\":\"" + item["CategoryGuid"].ToString() + "\",\"CategoryName\":\"" + item["CategoryName"].ToString() + "\",\"SubCategory\":" + "[]" + "},";
                    //}

                    if (!string.IsNullOrEmpty(datastr))
                        datastr = datastr.Substring(1);

                    return "{\"Succeed\":\"true\",\"Message\": \"\", \"Count\": \"" + ds.Tables[0].Rows.Count + "\", \"Data\":[" + datastr + "]}";
                }
            }
            catch (Exception ex)
            {
                LogService.WriteLog("查询所有栏目： " + ex.ToString(), "WCM_Error", LogService.LogLevel.Error);
                return "{\"Succeed\":\"false\",\"Message\": \"" + ex.ToString() + "\"}";
            }
        }

        /// <summary>
        /// 查询所有栏目
        /// </summary>
        /// <returns></returns>
        private static string GetAllChannelistAppkiz()
        {
            string datastr = "";

            try
            {
                using (SqlConnection con = new SqlConnection(ConstantValue.ConnectionString))
                {
                    con.Open();
                    string sql = @"SELECT [CategoryGuid]
                                          ,[CategoryName]
                                          ,[OrderNum]
                                      FROM [dbo].[WebDB_Category]
                                      WHERE Show=1
                                      ORDER BY OrderNum ASC ";

                    SqlCommand cmd = new SqlCommand(sql, con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        datastr += "{\"CategoryGuid\":\"" + item["CategoryGuid"].ToString() + "\",\"CategoryName\":\"" + item["CategoryName"].ToString() + "\",\"OrderNum\":\"" + item["OrderNum"].ToString() + "\"},";
                    }

                    if (!string.IsNullOrEmpty(datastr))
                        datastr = datastr.Substring(0, datastr.Length - 1);

                    return "{\"Succeed\":\"true\",\"Message\": \"\", \"Count\": \"" + ds.Tables[0].Rows.Count + "\", \"Data\":[" + datastr + "]}";
                }
            }
            catch (Exception ex)
            {
                LogService.WriteLog("查询所有栏目： " + ex.ToString(), "WCM_Error", LogService.LogLevel.Error);
                return "{\"Succeed\":\"false\",\"Message\": \"" + ex.ToString() + "\"}";
            }
        }

        /// <summary>
        /// 查询所有幻灯片
        /// </summary>
        /// <returns></returns>
        private static string GetSliderListAppkiz()
        {
            string datastr = "";

            try
            {
                using (SqlConnection con = new SqlConnection(ConstantValue.ConnectionString))
                {
                    con.Open();
                    string sql = @"SELECT [ID],[Title],[Url],[FilePath] FROM [dbo].[WebDB_Slider] ORDER BY OrderNum";

                    SqlCommand cmd = new SqlCommand(sql, con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        datastr += "{\"ID\":\"" + item["ID"].ToString() + "\",\"Title\":\"" + item["Title"].ToString() + "\",\"Url\":\"" + item["Url"].ToString() + "\",\"FilePath\":\"" + item["FilePath"].ToString() + "\"},";
                    }

                    if (!string.IsNullOrEmpty(datastr))
                        datastr = datastr.Substring(0, datastr.Length - 1);

                    return "{\"Succeed\":\"true\",\"Message\": \"\", \"Count\": \"" + ds.Tables[0].Rows.Count + "\", \"Data\":[" + datastr + "]}";
                }
            }
            catch (Exception ex)
            {
                LogService.WriteLog("查询所有幻灯片出错： " + ex.ToString(), "WCM_Error", LogService.LogLevel.Error);
                return "{\"Succeed\":\"false\",\"Message\": \"" + ex.ToString() + "\"}";
            }
        }

        /// <summary>
        /// 查询所有部门
        /// </summary>
        /// <returns></returns>
        private static string GetAllDeptListAppkiz(bool isDangZhengBanFirst = false)
        {
            string datastr = "";

            try
            {
                using (SqlConnection con = new SqlConnection(ConstantValue.ConnectionString))
                {
                    con.Open();

                    string sql = @"select DeptID,DeptName,DeptHierarchyCode from ORG_Department where (ParentID='0' and ParentID != '') Order by DeptHierarchyCode";

                    if (isDangZhengBanFirst)
                    {
                        // DeptHierarchyCode > '00006' 党政办前面的部门全部排出
                        sql = @"select DeptID,DeptName,DeptHierarchyCode from ORG_Department where (ParentID='0' and ParentID != '' AND DeptHierarchyCode > '00006') Order by DeptHierarchyCode";
                    }

                    SqlCommand cmd = new SqlCommand(sql, con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        datastr += "{\"DeptID\":\"" + item["DeptID"].ToString() + "\",\"DeptName\":\"" + item["DeptName"].ToString() + "\",\"DeptHierarchyCode\":\"" + item["DeptHierarchyCode"].ToString() + "\"},";
                    }

                    if (!string.IsNullOrEmpty(datastr))
                        datastr = datastr.Substring(0, datastr.Length - 1);

                    return "{\"Succeed\":\"true\",\"Message\": \"\", \"Count\": \"" + ds.Tables[0].Rows.Count + "\", \"Data\":[" + datastr + "]}";
                }
            }
            catch (Exception ex)
            {
                LogService.WriteLog("查询所有部门出错： " + ex.ToString(), "WCM_Error", LogService.LogLevel.Error);
                return "{\"Succeed\":\"false\",\"Message\": \"" + ex.ToString() + "\"}";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emplId"></param>
        /// <returns></returns>
        private static string EmplIdToName(string emplId)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(emplId))
            {
                string[] emplIds = emplId.Split(',');
                for (int i = 0; i < emplIds.Length; i++)
                {

                    if (result == "")
                    {
                        result = new OrgMgr().TryGetEmplName(emplIds[i]);
                    }
                    else
                    {
                        result += "," + new OrgMgr().TryGetEmplName(emplIds[i]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userPwd"></param>
        /// <returns></returns>
        private static string LoginSite(string userName, string userPwd)
        {
            var result = string.Empty;

            var context = HttpContext.Current;

            OrgMgr orgMgr = new OrgMgr();
            LogService.WriteLog("LoginSite, userName: ---" + userName, "WCM_Debug", LogService.LogLevel.Debug);
            LogService.WriteLog("LoginSite, userPwd: ---" + userPwd, "WCM_Debug", LogService.LogLevel.Debug);

            var user = orgMgr.GetUser(userName);
            if (user != null)
            {
                //密码加密进行比较
                var e_pwd = string.Empty;
                if (!string.IsNullOrWhiteSpace(userPwd))
                    e_pwd = Encryption.Encrypt(userPwd);
                if (e_pwd.Equals(user.UserPwd))
                {
                    if (user.UserDisabled == 0)
                    {
                        Appkiz.Library.Security.HttpModule.AuthModule am = (Appkiz.Library.Security.HttpModule.AuthModule)context.ApplicationInstance.Modules["AuthModule"];
                        if (am != null)
                        {
                            string ipAddress = context.Request.ServerVariables["REMOTE_ADDR"];
                            string returnUserName = "";
                            Appkiz.Library.Security.Authentication.LogonResultInfo logonResult = am.Login(user.UserName, Appkiz.Library.Common.Encryption.Decrypt(user.UserPwd), ipAddress, out returnUserName, null);
                            LogService.WriteLog("LoginSite", "logonResult:" + logonResult.Code, "WCM_Debug", LogService.LogLevel.Debug);
                            switch (logonResult.Code)
                            {
                                case LOGON_RESULT.SUCCEED_CHG_PWD:
                                case LOGON_RESULT.SUCCEED_PWD_EXPIRED:
                                case LOGON_RESULT.SUCCEED_PWD_TOO_SHORT:
                                case LOGON_RESULT.SUCCEED:

                                    var cookie = context.Response.Cookies["APPKIZTICKET"];
                                    if (cookie != null)
                                    {
                                        //清空缓存
                                        Appkiz.Library.Runtime.Product.Refresh();
                                        result = cookie.Value;
                                        return result;
                                    }
                                    break;
                                default:
                                    LogService.WriteLog("LoginSite，登录失败，登录用户名：" + userName + "，userName：" + userName + "，登录结果：" + logonResult.Code, "WCM_Debug", LogService.LogLevel.Debug);
                                    result = "登录失败，登录用户名：" + userName + "，登录结果：" + logonResult.Code;
                                    return "";
                            }
                        }
                        else
                        {
                            LogService.WriteLog("LoginSite，登录用户名：" + userName + "，未成功加载登录模块。", "WCM_Debug", LogService.LogLevel.Debug);
                            result = "登录失败，未成功加载登录模块";
                        }
                    }
                }
                else
                {
                    LogService.WriteLog("LoginSite，用户密码错误： " + userName, "WCM_Debug", LogService.LogLevel.Debug);
                    result = "用户密码错误";
                }
            }
            else
            {
                LogService.WriteLog("LoginSite，未找到用户： " + userName, "WCM_Debug", LogService.LogLevel.Debug);
                result = "未找到用户： " + userName;
            }

            return result;
        }

        /// <summary>
        /// 部门列表
        /// </summary>
        /// <param name="F_ID"></param>
        /// <param name="first"></param>
        /// <returns></returns>
        private static string GetDepartments(string F_ID, string first)
        {
            string result = ""; string datastr = "";
            try
            {
                using (SqlConnection con = new SqlConnection(ConstantValue.ConnectionString))
                {
                    con.Open();
                    string sql = string.Empty;
                    if (!string.IsNullOrEmpty(F_ID))
                        sql = "select DeptID,DeptName,ParentID,DeptHierarchyCode from ORG_Department where (ParentID=@fid and ParentID != '') Order by DeptHierarchyCode";
                    else
                        sql = "select DeptID,DeptName,ParentID,DeptHierarchyCode from ORG_Department where (ParentID='0' and ParentID != '') Order by DeptHierarchyCode";

                    SqlCommand cmd = new SqlCommand(sql, con);

                    if (!string.IsNullOrEmpty(F_ID))
                        cmd.Parameters.Add(new SqlParameter("@fid", F_ID));

                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    //  {"MenuName": "主任室","subMenu": [ ]}
                    //拼接
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        if (datastr != "")
                        {
                            datastr += ",";
                        }
                        datastr += "{\"MenuID\":\"" + item["DeptHierarchyCode"].ToString() + "\",\"MenuName\":\"" + item["DeptName"].ToString() + "\",\"subMenu\":[" + GetDepartments(item["DeptID"].ToString(), "F") + "]}";
                    }
                    if (first == "T")
                    {
                        result += "{\"data\":{\"menu\":[" + datastr + "]}}";
                    }
                    else
                    {
                        result = datastr;
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                return "{\"Succeed\":\"false\",\"Message\": \"" + ex.ToString() + "\"}";
            }

        }

        /// <summary>
        /// 查询通讯录
        /// </summary>
        /// <param name="deptid"></param>
        /// <param name="name"></param>
        /// <param name="phone"></param
        /// <param name="tel"></param>
        /// <returns></returns>
        private static string GetContactInfos(string deptid, string name, string phone, string tel)
        {
            string datastr = "";

            try
            {
                using (SqlConnection con = new SqlConnection(ConstantValue.ConnectionString))
                {
                    con.Open();
                    string sql = "";

                    if ("0" == deptid)
                    {
                        sql = @"select e.EmplID,e.EmplName,d.DeptID,d.DeptName,u.UserName from ORG_Employee e,ORG_Department d,ORG_User u,ORG_EmplDept ed
                    where e.EmplID=ed.EmplID and ed.DeptID=d.DeptID and e.EmplID=u.EmplID and (e.EmplName like @name or e.PinyinAbbr like @szm or u.UserName like @name) and d.DeptHierarchyCode like @deptid order by e.GlobalSortNo desc";
                    }
                    else
                    {
                        sql = @"select e.EmplID,e.EmplName,d.DeptID,d.DeptName,u.UserName from ORG_Employee e,ORG_Department d,ORG_User u,ORG_EmplDept ed
                    where e.EmplID=ed.EmplID and ed.DeptID=d.DeptID and e.EmplID=u.EmplID and (e.EmplName like @name or e.PinyinAbbr like @szm or u.UserName like @name) and d.DeptHierarchyCode like @deptid order by d.DeptHierarchyCode asc,ed.SortEmplInDept asc,e.EmplID asc";
                    }

                    SqlCommand cmd = new SqlCommand(sql, con);
                    cmd.Parameters.Add(new SqlParameter("@name", "%" + name + "%"));
                    cmd.Parameters.Add(new SqlParameter("@szm", name + "%"));
                    cmd.Parameters.Add(new SqlParameter("@deptid", deptid + "%"));

                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    var info = string.Empty;
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        if (string.IsNullOrEmpty(phone))
                        {
                            //为空
                            //if (datastr != "" && datastr != null)
                            //{
                            //    datastr += ",";
                            //}

                            if (string.IsNullOrEmpty(tel))
                            {
                                info = GetEmplInfo(item["EmplID"].ToString(), "", "");
                            }
                            else
                            {
                                info = GetEmplInfo(item["EmplID"].ToString(), "", tel);
                            }

                            if (info != "false")
                            {
                                //不为空
                                if (datastr != "" && datastr != null)
                                {
                                    datastr += ",";
                                }

                                datastr += "{\"Emplid\":\"" + item["EmplID"].ToString() + "\",\"EmplName\":\"" + item["EmplName"].ToString() + "\",\"Deptid\":\"" + item["DeptID"].ToString() + "\",\"deptname\":\"" + item["DeptName"].ToString() + "\"" + info + "}";
                            }
                        }
                        else if (!string.IsNullOrEmpty(phone))
                        {
                            if (string.IsNullOrEmpty(tel))
                            {
                                info = GetEmplInfo(item["EmplID"].ToString(), phone, "");
                            }
                            else
                            {
                                info = GetEmplInfo(item["EmplID"].ToString(), phone, tel);
                            }

                            if (info != "false")
                            {
                                //不为空
                                if (datastr != "" && datastr != null)
                                {
                                    datastr += ",";
                                }
                                datastr += "{\"Emplid\":\"" + item["EmplID"].ToString() + "\",\"EmplName\":\"" + item["EmplName"].ToString() + "\",\"Deptid\":\"" + item["DeptID"].ToString() + "\",\"deptname\":\"" + item["DeptName"].ToString() + "\"" + info + "}";
                            }
                        }
                    }

                    return "{\"Succeed\":\"true\",\"Message\": \"\",\"Data\":[" + datastr + "]}"; ;
                }
            }
            catch (Exception ex)
            {
                LogService.WriteLog("CXTXL(string deptid, string name, string phone)出错： " + ex.ToString(), "WCM_Error", LogService.LogLevel.Error);
                return "{\"Succeed\":\"false\",\"Message\": \"" + ex.ToString() + "\"}";
            }
        }

        private static string GetEmplInfo(string emplid, string phone, string telFilter)
        {
            string tel = ""; string fjh = ""; string mobile = ""; string email = "";//2018年6月7日
            try
            {
                using (SqlConnection con = new SqlConnection(ConstantValue.ConnectionString))
                {
                    con.Open();
                    string sql = "";
                    sql = @"select EmplID,ContactInfoTypeID,ContactInfoValue from ORG_EmployeeContactInfo where EmplID=@id";
                    SqlCommand cmd = new SqlCommand(sql, con);
                    cmd.Parameters.Add(new SqlParameter("@id", emplid));
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        string type = item["ContactInfoTypeID"].ToString();
                        string val = item["ContactInfoValue"].ToString();
                        if (type == "tel")
                        {
                            var fenjihao = "";
                            if (val.Contains("Key=\"fenjihao\""))
                            {
                                fenjihao = val.Substring(val.IndexOf("fenjihao") + 17);
                                fenjihao = fenjihao.Substring(0, fenjihao.IndexOf("\""));
                            }

                            val = val.Substring(val.IndexOf("tel") + 12);
                            val = val.Substring(0, val.IndexOf("\""));

                            tel += val + ";";                                   //2018年6月7日
                            fjh += (fenjihao == "" ? "空" : fenjihao) + ";";     //2018年6月7日
                                                                                //tel += "\"tel\":\""+val+"\"";
                        }
                        else if (type == "mobile")
                        {
                            val = val.Substring(val.IndexOf("mobile") + 15);
                            val = val.Substring(0, val.IndexOf("\""));
                            mobile += val + ";";
                        }
                        else if (type == "email")
                        {
                            val = val.Substring(val.IndexOf("email") + 14);
                            val = val.Substring(0, val.IndexOf("\""));
                            email += val + ";";
                        }
                    }

                    if (phone == "" || phone == null)
                    {
                        if (telFilter == "" || telFilter == null)
                        {
                            return ",\"tel\":\"" + tel + "\"" + ",\"fjh\":\"" + fjh + "\"" + ",\"mobile\":\"" + mobile + "\"" + ",\"email\":\"" + email + "\""; //2018年6月7日
                        }
                        else
                        {
                            int t = tel.IndexOf(telFilter);
                            if (t >= 0)
                            {
                                return ",\"tel\":\"" + tel + "\"" + ",\"fjh\":\"" + fjh + "\"" + ",\"mobile\":\"" + mobile + "\"" + ",\"email\":\"" + email + "\""; //2018年6月7日
                            }
                            else
                            {
                                return "false";
                            }
                        }
                    }
                    else
                    {
                        int a = mobile.IndexOf(phone);
                        if (a >= 0)
                        {
                            if (telFilter == "" || telFilter == null)
                            {
                                return ",\"tel\":\"" + tel + "\"" + ",\"fjh\":\"" + fjh + "\"" + ",\"mobile\":\"" + mobile + "\"" + ",\"email\":\"" + email + "\""; //2018年6月7日
                            }
                            else
                            {
                                int t = tel.IndexOf(telFilter);
                                if (t >= 0)
                                {
                                    return ",\"tel\":\"" + tel + "\"" + ",\"fjh\":\"" + fjh + "\"" + ",\"mobile\":\"" + mobile + "\"" + ",\"email\":\"" + email + "\""; //2018年6月7日
                                }
                                else
                                {
                                    return "false";
                                }
                            }
                        }
                        else
                        {
                            return "false";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogService.WriteLog("GetEmplInfo(string emplid, string phone, string telFilter)出错： " + ex.ToString(), "WCM_Error", LogService.LogLevel.Error);
                return "{\"Succeed\":\"false\",\"Message\": \"" + ex.ToString() + "\"}";
            }

        }
        #endregion

    }
}