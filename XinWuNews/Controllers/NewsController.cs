﻿using Appkiz.Library.Security.Authentication;
using Biz;
using Biz.Constant;
using Biz.Util;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XinWuNews.Models.News;

namespace XinWuNews.Controllers
{
    public class NewsController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: News
        public ActionResult Index(string categoryGuid, string infoId)
        {
            var result = new NewsModel();

            ViewData["menu"] = "home";

            try
            {
                var identity = User.Identity as AppkizIdentity;

                var roleBiz = new RoleBiz();

                var roleIdList = new List<string>();

                if (identity != null)
                {
                    roleIdList = roleBiz.GetRoleIdList(identity.Employee.EmplID);
                }

                var categoryBiz = new CatogeryBiz();

                var categoryPermission = categoryBiz.GetCategoryPermission(infoId);

                if (string.IsNullOrEmpty(categoryPermission) || categoryPermission == "ALL" || roleIdList.Count == 0 || roleIdList.Exists(x => categoryPermission.Contains(x)))
                {
                    var infoBiz = new InfomationBiz();

                    if (identity == null && infoBiz.GetNewsPermission(infoId)
                        || identity != null && infoBiz.GetNewsPermission(infoId, identity.Employee.EmplID))
                    {
                        var biz = new InfomationBiz();

                        var newsDetailDto = biz.GetDetailNews(categoryGuid, infoId);

                        if (newsDetailDto != null)
                        {
                            FastCopy.Copy<NewsDetailDto, NewsModel>(newsDetailDto, result);

                            var mobileBiz = new MobileBiz();

                            result.AttachInfos = mobileBiz.GetAttachInfo(infoId);

                            biz.UpdateClickTimes(infoId);
                        }
                        else
                        {
                            result.AttachInfos = new List<AttachInfoDto>();
                        }

                        result.IsPermission = true;
                    }
                    else
                    {
                        result.IsPermission = false;
                        result.AttachInfos = new List<AttachInfoDto>();
                    }
                }
                else
                {
                    result.IsPermission = false;
                    result.AttachInfos = new List<AttachInfoDto>();
                }

                result.I8NewsServer = ConstantValue.I8NewsServer;
            }
            catch (Exception ex)
            {
                result.AttachInfos = new List<AttachInfoDto>();

                logger.Error(ex);
            }

            return View(result);
        }

        public ActionResult MobilePage(string infoId)
        {
            var result = new NewsModel();

            try
            {
                var biz = new InfomationBiz();

                var newsDetailDto = biz.GetMobileDetailNews(infoId);

                FastCopy.Copy<NewsDetailDto, NewsModel>(newsDetailDto, result);

                var mobileBiz = new MobileBiz();

                result.AttachInfos = mobileBiz.GetAttachInfo(infoId);

                //点击次数+1
                biz.UpdateClickTimes(infoId);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return View(result);
        }
    }
}