﻿using Appkiz.Library.Security.Authentication;
using Biz;
using Biz.Constant;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XinWuNews.Models.Feedback;

namespace XinWuNews.Controllers
{
    public class FeedbackApiController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        /// <summary>
        /// 取得反馈内容
        /// </summary>
        /// <returns></returns>
        [Route("api/feeedback/get/")]
        public IHttpActionResult GetFeedback(FeedbackReqModel model)
        {
            var result = new ApiResponseVo<FeedbackDto>();

            try
            {
                var identity = User.Identity as AppkizIdentity;

                var emplId = string.Empty;

                //var isHasDeletePermission = false;

                if (identity != null && identity.Employee != null)
                {
                    emplId = identity.Employee.EmplID;
                }

                //var roleIds = new List<string>();

                //if (!string.IsNullOrEmpty(emplId))
                //{
                //    var roleBiz = new RoleBiz();
                //    roleIds = roleBiz.GetRoleIdList(emplId);
                //}

                //if (roleIds.Contains(ConstantValue.AdminRoleId))
                //{
                //    isHasDeletePermission = true;
                //}

                var biz = new FeedBackBiz();

                result.Data = biz.GetFeedback(model.InfoId);

                foreach (var item in result.Data)
                {
                    if (!string.IsNullOrEmpty(emplId) && item.OUGuid == emplId)
                    {
                        item.isHasDeletePermission = true;
                    }
                    else
                    {
                        item.isHasDeletePermission = false;
                    }
                }

                result.Count = result.Data.Count;

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 提交反馈内容
        /// </summary>
        /// <returns></returns>
        [Route("api/feeedback/post/")]
        public IHttpActionResult PostFeedback(FeedbackReqModel model)
        {
            var result = new ApiResponseStrVo();

            try
            {
                var identity = User.Identity as AppkizIdentity;

                if (identity != null && identity.Employee != null)
                {
                    var emplBiz = new EmployeeBiz();

                    var emplNmae = emplBiz.GetEmplName(identity.Employee.EmplID);

                    var sensitivityBiz = new SensitivityBiz();

                    var list = sensitivityBiz.GetSensitivity();

                    foreach (var item in list)
                    {
                        model.Content = model.Content.Replace(item.Name, item.Replacement);
                    }

                    var biz = new FeedBackBiz();

                    biz.DeleteFeedbackByEmplid(model.InfoId, identity.Employee.EmplID);

                    result.Data = biz.SaveFeedback(emplNmae, model.Content, model.InfoId, model.SysGuid, identity.Employee.EmplID).ToString();
                }
                else
                {
                    result.Data = string.Empty;
                }

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        [HttpPost]
        /// <summary>
        /// 删除反馈内容
        /// </summary>
        /// <returns></returns>
        [Route("api/feeedback/delete/")]
        public IHttpActionResult DeleteFeedback(FeedbackReqModel model)
        {
            var result = new ApiResponseStrVo();

            try
            {
                var emplId = (User.Identity as AppkizIdentity).Employee.EmplID;

                var roleBiz = new RoleBiz();

                var roleIds = roleBiz.GetRoleIdList(emplId);

                if (roleIds.Contains(ConstantValue.AdminRoleId))
                {
                    var biz = new FeedBackBiz();

                    result.Data = biz.DeleteFeedback(model.FeedbackId).ToString();
                }
                else
                {
                    result.Data = string.Empty;
                }

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }
    }
}