﻿using Biz;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XinWuNews.Models.Catogery;

namespace XinWuNews.Controllers
{
    public class SearchController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ActionResult Index(string keyword = "", string startTime = "", string endTime = "", string categoryGuid = "",
            int pageIndex = 0, string emplId = "", string deptId = "")
        {

            var result = new AdvancedSearchModel();

            ViewData["menu"] = "home";

            try
            {
                result.StartDate = startTime;

                result.EndDate = endTime;

                result.CategoryList = new List<AdvancedSearchDto>();

                result.Keyword = keyword;

                //if (!string.IsNullOrEmpty(keyword))
                //{
                //    result = Search(keyword, startTime, endTime, categoryGuid, pageIndex, emplId, deptId);
                //}
                //else
                //{
                    
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return View(result);
        }

        #region 后台渲染，弃用
        //public ActionResult ClickSearch(string keyword = "", string startTime = "", string endTime = "", string categoryGuid = "",
        //        int pageIndex = 0, string emplId = "", string deptId = "")
        //{

        //    var result = new AdvancedSearchModel();

        //    ViewData["menu"] = "home";

        //    try
        //    {
        //        result = Search(keyword, startTime, endTime, categoryGuid, pageIndex, emplId, deptId);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex);
        //    }

        //    return View("Index", result);
        //}

        //private AdvancedSearchModel Search(string keyword, string startTime, string endTime, string categoryGuid, int pageIndex, string emplId, string deptId)
        //{
        //    var result = new AdvancedSearchModel();

        //    try
        //    {
        //        var biz = new CatogeryBiz();

        //        result.CategoryGuid = categoryGuid;

        //        result.Keyword = keyword;

        //        if (" - " == startTime)
        //        {
        //            startTime = string.Empty;
        //        }

        //        if (" - " == endTime)
        //        {
        //            endTime = string.Empty;
        //        }

        //        result.StartDate = startTime;

        //        result.EndDate = endTime;

        //        if (!string.IsNullOrEmpty(endTime))
        //        {
        //            DateTime endDate = DateTime.Today;
        //            DateTime.TryParse(endTime, out endDate);
        //            endDate = endDate.AddDays(1);

        //            endTime = endDate.ToString("yyyy-MM-dd");
        //        }

        //        if (!string.IsNullOrEmpty(categoryGuid))
        //        {
        //            //1,2,3 => '1','2','3' SQL IN 参数化
        //            categoryGuid = "'" + categoryGuid.Replace(",", "','") + "'";
        //        }

        //        if (!string.IsNullOrEmpty(deptId))
        //        {
        //            //1,2,3 => '1','2','3' SQL IN 参数化
        //            deptId = "'" + deptId.Replace(",", "','") + "'";
        //        }

        //        result.TotalCount = biz.AdvancedSearchTotalCount(keyword, categoryGuid, startTime, endTime, emplId, deptId);

        //        result.TotalPage = (result.TotalCount + 20) / 20;

        //        result.PageIndex = pageIndex;

        //        pageIndex = pageIndex == 0 ? 1 : pageIndex;

        //        result.CategoryList = biz.AdvancedSearchCatogeryNews(keyword, categoryGuid, startTime, endTime, (pageIndex - 1) * 20, pageIndex * 20, 49, emplId, deptId);


        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex);
        //    }

        //    return result;
        //}

#endregion

    }
}