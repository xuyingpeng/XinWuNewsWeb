﻿using Appkiz.Library.Security.Authentication;
using Biz;
using Biz.Constant;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace XinWuNews.Controllers
{
    public class MobileController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <returns></returns>
        [Route("api/mobile/get_news_list/")]
        public IHttpActionResult GetCatogery(string catogeryName, string __session__="", int pageIndex = 0, int pageSize = 15, string keyword = "")
        {
            var result = new PageResponseVo<MobileCategoryDto>();
            try
            {
                if (string.IsNullOrEmpty(keyword))
                {
                    keyword = string.Empty;
                }

                var employee = (User.Identity as AppkizIdentity).Employee;

                var emplId = employee.EmplID;

                if (ConstantValue.DicCategoryNameMap.ContainsKey(catogeryName))
                {
                    catogeryName = ConstantValue.DicCategoryNameMap[catogeryName];
                }

                var biz = new MobileBiz();

                var categoryGuid = biz.GetCategoryGuid(catogeryName);

                if (string.IsNullOrEmpty(categoryGuid))
                {
                    result.TotalCount = 0;
                    result.Data = new List<MobileCategoryDto>();
                }
                else
                {
                    result.TotalCount = biz.GetTotalCount(categoryGuid, emplId, keyword);

                    var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                    result.Data = biz.GetCategoryList(categoryGuid, pageIndex * pageSize, (pageIndex + 1) * pageSize, 49, emplId, keyword, baseUrl);
                }

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <returns></returns>
        [Route("api/mobile/get_attach_info/")]
        public IHttpActionResult GetAttachInfo(string infoId, string __session__ = "")
        {
            var result = new PageResponseVo<AttachInfoDto>();
            try
            {
                var infoBiz = new InfomationBiz();

                var newsDetailDto = infoBiz.GetMobileDetailNews(infoId);

                if (newsDetailDto != null
                    && !string.IsNullOrEmpty(newsDetailDto.InfoContent)
                    && (newsDetailDto.InfoContent.Contains("<img")
                        || newsDetailDto.InfoContent.Contains("<IMG")))
                {
                    result.Data = new List<AttachInfoDto>();
                }
                else
                {
                    var biz = new MobileBiz();

                    result.Data = biz.GetAttachInfo(infoId);

                    result.TotalCount = result.Data != null ? result.Data.Count : 0;
                }

                result.Succeed = true;
            }
            catch (Exception ex)
            {
                result.Succeed = false;

                logger.Error(ex);
            }

            return Ok(result);
        }

    }
}