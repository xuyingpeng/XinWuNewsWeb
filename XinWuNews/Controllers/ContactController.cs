﻿using Biz;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XinWuNews.Models.Contact;

namespace XinWuNews.Controllers
{
    public class ContactController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ActionResult Index()
        {
            ViewData["menu"] = "contact";

            return View();
        }


        public ActionResult Search(string deptid = "f9c57c95-55f5-475c-a75f-ea5d92aa0bdd", string name="", string phone="")
        {
            var result = new ContactModel();

            ViewData["menu"] = "contact";

            try
            {
                //var biz = new AppkizBiz();

                //result.ContactInfos = biz.GetContactInfos(deptid, name, phone);

                //result.Count = result.ContactInfos.Count;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return PartialView(result);
        }
    }
}