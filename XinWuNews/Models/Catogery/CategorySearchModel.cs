﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.Catogery
{
    public class CategorySearchModel
    {
        public string keyword { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string categoryGuid { get; set; }
        public int pageIndex { get; set; }
        public string emplId { get; set; }
        public string deptId { get; set; }
        public string menudeptId { get; set; }
    }
}