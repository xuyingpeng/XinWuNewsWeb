﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.Catogery
{
    public class AdvancedSearchModel
    {
        public string Keyword { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string CategoryGuid { get; set; }

        public string DeparmentId { get; set; }

        public int PageIndex { get; set; }

        public int TotalCount { get; set; }

        public int TotalPage { get; set; }

        public List<AdvancedSearchDto> CategoryList { get; set; }
    }
}