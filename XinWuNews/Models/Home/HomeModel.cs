﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.Home
{
    public class HomeModel
    {
        /// <summary>
        /// 重要新闻
        /// </summary>
        public List<HomeNewsDto> ImportantNews { get; set; }

        /// <summary>
        /// 重要新闻
        /// </summary>
        public List<HomeNewsDto> ImportantNotice { get; set; }

        /// <summary>
        /// 新吴动态
        /// </summary>
        public List<HomeNewsDto> XinWuDynamic { get; set; }

        /// <summary>
        /// 社区动态
        /// </summary>
        public List<HomeNewsDto> CommunityDynamic { get; set; }

        /// <summary>
        /// 大走访活动动态
        /// </summary>
        public List<HomeNewsDto> VisitDynamicActivities { get; set; }

        /// <summary>
        /// 优选信息
        /// </summary>
        public List<HomeNewsDto> OptimizingInformation { get; set; }

        /// <summary>
        /// 部门通知
        /// </summary>
        public List<HomeNewsDto> DepartmentNotice { get; set; }

        /// <summary>
        /// 重要文件
        /// </summary>
        public List<HomeNewsDto> ImportantDocuments { get; set; }

        /// <summary>
        /// 部门文件
        /// </summary>
        public List<HomeNewsDto> DepartmentDocuments { get; set; }

        /// <summary>
        /// 公示公告
        /// </summary>
        public List<HomeNewsDto> Announcement { get; set; }

        /// <summary>
        /// 一周工作计划
        /// </summary>
        public List<HomeNewsDto> WeeklySchedule { get; set; }

        /// <summary>
        /// 解放思想大讨论
        /// </summary>
        public List<HomeNewsDto> EmancipatesDiscussion { get; set; }

        /// <summary>
        /// 263在行动
        /// </summary>
        public List<HomeNewsDto> Action263 { get; set; }

    }
}