﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.Feedback
{
    public class FeedbackReqModel
    {
        public string EmplId { get; set; }

        public string InfoId { get; set; }

        public string Content { get; set; }

        public string SysGuid { get; set; }

        public string FeedbackId { get; set; }
    }
}