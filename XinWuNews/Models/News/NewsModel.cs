﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.News
{
    public class NewsModel
    {
        /// <summary>
        /// 新闻Guid
        /// </summary>
        public string SysGuid { get; set; }

        /// <summary>
        /// 新闻ID
        /// </summary>
        public string InfoId { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string TitleColor { get; set; }

        /// <summary>
        /// 栏目ID
        /// </summary>
        public string CategoryGuid { get; set; }

        /// <summary>
        /// 栏目名称
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PubInWebDate { get; set; }

        /// <summary>
        /// 页面显示时间
        /// </summary>
        public string DisplayDate { get; set; }

        /// <summary>
        /// 浏览次数
        /// </summary>
        public int ClickTimes { get; set; }

        /// <summary>
        /// 撰稿人
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 核稿人
        /// </summary>
        public string Auditor { get; set; }

        /// <summary>
        /// 部门
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 新闻内容
        /// </summary>
        public string InfoContent { get; set; }

        /// <summary>
        /// 是否显示（审核，撰稿，部门）
        /// </summary>
        public bool IsDisplayAll { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        public List<AttachInfoDto> AttachInfos { get; set; }

        /// <summary>
        /// 附件相关服务Server
        /// </summary>
        public string I8NewsServer { get; set; }

        /// <summary>
        /// 是否允许评论
        /// </summary>
        public bool IsFeedBack { get; set; }

        /// <summary>
        /// 是否有权限查看
        /// </summary>
        public bool IsPermission { get; set; }
    }
}