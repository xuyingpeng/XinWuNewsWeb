﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.AppkizApi
{
    public class KeyValueModel
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}