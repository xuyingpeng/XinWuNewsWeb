﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.Contact
{
    public class ContactModel
    {
        public List<ContactInfoDto> ContactInfos { get; set; }

        public int Count { get; set; }
    }
}