﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XinWuNews.Models.Contact
{
    public class ContactInfoModel
    {
        public string deptid { get; set; }

        public string name { get; set; }

        public string phone { get; set; }

        public string tel { get; set; }
    }
}