﻿using Biz.Constant;
using Biz.Util;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz
{
    public class InfomationBiz
    {
        /// <summary>
        /// 新闻详情
        /// </summary>
        /// <returns></returns>
        public NewsDetailDto GetDetailNews(string categoryGuid, string infoId)
        {
            var result = new NewsDetailDto();

            var sql = @"SELECT  
                                T3.CategoryGuid AS SysGuid,
                                T2.InfoID,
	                            T2.Title,
	                            T2.TitleColor,
	                            T2.ClickTimes,
                                T2.Writer AS Author,
                                T2.Auditor,
	                            ISNULL(T4.DeptName,T2.[DeptId]) AS Department,
                                T2.InfoContent,
                                T2.[PubInWebDate],
                                T1.CategoryGuid,
                                T3.IsFeedBack,
                                T3.CategoryName
                        FROM    [dbo].[WebDB_Info_InCategory] T1
                                INNER JOIN [dbo].[WebDB_Information] T2
                                ON T1.[InfoID] = T2.[InfoID]
	                            INNER JOIN [dbo].[WebDB_Category] T3
                                ON T1.[CategoryGuid] = T3.[CategoryGuid]
                                LEFT JOIN [dbo].[ORG_Department] T4
                                ON T2.[DeptId] = T4.[DeptId]
	                    WHERE T1.[CategoryGuid] = @CategoryGuid
                        AND T1.[InfoID] = @InfoId";
            //                        AND T2.[EndDate] > GETDATE()

            result = DapperHelper.ExcuteQueryFirstOrDefault<NewsDetailDto>(sql, new { CategoryGuid = categoryGuid , InfoId = infoId });

            if (result != null)
            {
                result.DisplayDate = result.PubInWebDate.ToString("yyyy-MM-dd");

                if (ConstantValue.NewsDisplayAll.Contains(categoryGuid))
                {
                    result.IsDisplayAll = true;
                }
                else
                {
                    result.IsDisplayAll = false;
                }
            }

            return result;
        }

        /// <summary>
        /// 新闻详情
        /// </summary>
        /// <returns></returns>
        public NewsDetailDto GetMobileDetailNews(string infoId)
        {
            var result = new NewsDetailDto();

            var sql = @"SELECT  
                                T2.InfoID,
	                            T2.Title,
	                            T2.TitleColor,
	                            T2.ClickTimes,
                                T2.Writer AS Author,
                                T2.Auditor,
	                            '' AS Department,
                                T2.InfoContent,
                                T2.[PubInWebDate],
                                T3.IsFeedBack
                        FROM    [dbo].[WebDB_Information] T2,
                                [dbo].[WebDB_Category] T3,
                                [dbo].[WebDB_Info_InCategory] T1
	                    WHERE T1.[InfoID] = T2.[InfoID]
                        AND T1.[CategoryGuid] = T3.[CategoryGuid]
                        AND T2.[InfoID] = @InfoId";
            //                          AND T2.[EndDate] > GETDATE()

            result = DapperHelper.ExcuteQueryFirstOrDefault<NewsDetailDto>(sql, new { InfoId = infoId });

            if (result != null)
            {
                result.DisplayDate = result.PubInWebDate.ToString("yyyy-MM-dd");
            }

            return result;
        }


        /// <summary>
        /// 首页新闻列表
        /// </summary>
        /// <returns></returns>
        public List<HomeNewsDto> GetHomeNews(string categoryGuid, int topCount, int displayLimit, string defaultColor = "#444444")
        {
            var result = new List<HomeNewsDto>();

            var sql = @"SELECT  TOP {0}
                                T2.InfoID,
	                            T2.Title,
	                            T2.TitleColor,
                                T2.PubInWebDate,
                                T1.CategoryGuid
                        FROM    [dbo].[WebDB_Info_InCategory] T1,
	                            [dbo].[WebDB_Information] T2
	                    WHERE T1.[InfoID] = T2.[InfoID]
                          AND (T2.[EndDate] IS NULL OR T2.[EndDate] > GETDATE())
	                      AND T1.[CategoryGuid] = @CategoryGuid
                        ORDER BY T2.[OrderNum] ASC,T2.[PubInWebDate] DESC";                  

            sql = string.Format(sql, topCount.ToString());

            result = DapperHelper.ExcuteQuery<HomeNewsDto>(sql, new { CategoryGuid = categoryGuid });

            foreach (var item in result)
            {
                if (item.Title != null && item.Title.Length > displayLimit)
                {
                    item.DisplayTitle = item.Title.Substring(0, displayLimit) + "...";
                }
                else
                {
                    item.DisplayTitle = item.Title;
                }

                item.DisplayDate = item.PubInWebDate.ToString("MM-dd");

                if (string.IsNullOrEmpty(item.TitleColor) || item.TitleColor == "000000" || item.TitleColor == "#000000")
                {
                    item.TitleColor = defaultColor;
                }
            }

            return result;
        }

        public int UpdateClickTimes(string infoId)
        {
            var result = 0;

            var sql = @"UPDATE [dbo].[WebDB_Information]
                        SET [ClickTimes] = [ClickTimes] + 1
                        WHERE [InfoID]=@infoId";

            result = DapperHelper.ExcuteNonQuery(sql, new { InfoId = infoId});

            return result;
        }

        public bool GetNewsPermission(string infoId,string emplId)
        {
            var result = false;

            var sql = @"     SELECT 
                                    T2.[InfoID]
                            FROM    [dbo].[WebDB_Information] T2
		                    WHERE T2.[InfoID] = @InfoID
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '' OR T2.[ViewPermission] LIKE '%' + @EmplId + '%')
                                OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = '' $RoleIds ))";

            var roleIds = RoleBiz.GetRoleIdsLikeCondition(emplId);

            sql = sql.Replace("$RoleIds", roleIds);

            var dbResult = DapperHelper.ExcuteQuery<NewsPermissionDto>(sql, new { InfoID = infoId, EmplId = emplId });

            if (dbResult.Count > 0)
            {
                result = true;
            }

            return result;
        }

        public bool GetNewsPermission(string infoId)
        {
            var result = false;

            var sql = @"     SELECT 
                                    T2.[InfoID]
                            FROM    [dbo].[WebDB_Information] T2
		                    WHERE T2.[InfoID] = @InfoID
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '')
                                OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = ''))";

            var dbResult = DapperHelper.ExcuteQuery<NewsPermissionDto>(sql, new { InfoID = infoId });

            if (dbResult.Count > 0)
            {
                result = true;
            }

            return result;
        }
    }
}
