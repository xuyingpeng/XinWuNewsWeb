﻿using Biz.Util;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz
{
    public class RoleBiz
    {
        public static string GetRoleIdsLikeCondition(string emplId)
        {
            var result = string.Empty;

            var sql = @"     SELECT 
                                    [RoleID]
                            FROM    [dbo].[ORG_EmplRole]
		                    WHERE [EmplID] = @EmplID";

            var dbResult = DapperHelper.ExcuteQuery<RoleDto>(sql, new { EmplID = emplId });

            foreach (var roleDto in dbResult)
            {
                result = result + " OR " + string.Format("T2.[ViewRoles] LIKE '%{0}%'", roleDto.RoleId);
            }

            return result;
        }


        public List<string> GetRoleIdList(string emplId)
        {
            var result = new List<string>();

            var sql = @"     SELECT 
                                    [RoleID]
                            FROM    [dbo].[ORG_EmplRole]
		                    WHERE [EmplID] = @EmplID";

            var dbResult = DapperHelper.ExcuteQuery<RoleDto>(sql, new { EmplID = emplId });

            foreach (var roleDto in dbResult)
            {
                result.Add(roleDto.RoleId);
            }

            return result;
        }
    }
}
