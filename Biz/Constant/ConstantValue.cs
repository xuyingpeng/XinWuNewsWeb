﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz.Constant
{
    public class ConstantValue
    {
        public const int PageSize = 20;

        public const string CatogeryGuids = "'122003c8-0a47-4cb9-a867-1aa94e5144cd','24fbc0c8-77a9-4aab-b5c7-66ff03d7efa7','6cff411e-b8a2-4db4-8dc9-a32a5e65efee','8c3a6f8c-6c05-4a58-a324-c6ce2f6ecb0f','8c8174d4-95c0-43d0-b42b-413fb5166d06','96bb1b39-762b-4ddd-9f05-72d0c6f36146','9c9d5ea8-6478-483e-bfca-ecd6e8e288cc','bd32cd0b-3225-4700-a171-c57d453d1942','c412fa92-becf-4611-8695-6a35a830fad0','ca8d2c74-a4e6-4d93-b93b-04435688cae2','cd5c8bdb-e15e-4a57-8e00-8b0796cdf703','e279bf06-3412-4672-9fdd-d7f20e038eff','e53cc971-96f9-4ba7-8248-7e4ee7c13024','e78f26a6-3b38-4169-8ad6-8761be948a2f','f8ccf3cc-6f33-4c7c-9ddc-9183f568c947','fdf15892-5720-4e3e-a024-b17be87648de'";

        //重要新闻
        public const string ImportantNews = "ca8d2c74-a4e6-4d93-b93b-04435688cae2";

        //重要通知
        public const string ImportantNotice = "e279bf06-3412-4672-9fdd-d7f20e038eff";

        //新吴动态
        public const string XinWuDynamic = "e53cc971-96f9-4ba7-8248-7e4ee7c13024";

        //社区动态
        public const string CommunityDynamic = "8c8174d4-95c0-43d0-b42b-413fb5166d06";

        //解放思想大讨论动态
        public const string VisitDynamicActivities = "688bb4cc-060f-4a36-ae26-cfae3cb51748";

        //优选信息
        public const string OptimizingInformation = "9c9d5ea8-6478-483e-bfca-ecd6e8e288cc";

        //部门通知
        public const string DepartmentNotice = "e78f26a6-3b38-4169-8ad6-8761be948a2f";

        //重要文件
        public const string ImportantDocuments = "122003c8-0a47-4cb9-a867-1aa94e5144cd";

        //部门文件
        public const string DepartmentDocuments = "24fbc0c8-77a9-4aab-b5c7-66ff03d7efa7";

        //公示公告
        public const string Announcement = "cd5c8bdb-e15e-4a57-8e00-8b0796cdf703";

        //一周工作计划
        public const string WeeklySchedule = "c03f45cc-845d-4f31-9435-d3ca5f9f5a09";

        //解放思想大讨论 todo：没有定义
        public const string EmancipatesDiscussion = "be959639-221f-4e1d-82fa-367d7310f55a";

        //263在行动
        public const string Action263 = "c412fa92-becf-4611-8695-6a35a830fad0";

        //网上教程
        public const string OnlineCourses = "bd32cd0b-3225-4700-a171-c57d453d1942";

        //专题链接
        public const string SpecialLinks = "f8ccf3cc-6f33-4c7c-9ddc-9183f568c947";

        //专题链接 CategoryNum
        public const string SpecialLinksNum = "002";

        //督查工作
        public const string SupervisionWork = "8c3a6f8c-6c05-4a58-a324-c6ce2f6ecb0f";

        //应急值守
        public const string EmergencyDuty = "96bb1b39-762b-4ddd-9f05-72d0c6f36146";

        //应急预案
        public const string EmergencyPlan = "fdf15892-5720-4e3e-a024-b17be87648de";

        //应急指南
        public const string EmergencyGuideline = "6cff411e-b8a2-4db4-8dc9-a32a5e65efee";

        //Admin Role Id
        public const string AdminRoleId = "ADMIN";

        /// <summary>
        /// 是否显示（审核，撰稿，部门）
        /// </summary>
        public static readonly List<string> NewsDisplayAll = new List<string>();

        /// <summary>
        /// 附件预览接口服务域名
        /// </summary>
        public static readonly string I8NewsServer;

        /// <summary>
        /// 个人办公URL
        /// </summary>
        public static readonly string PrivateOfficeUrl;

        public static readonly string ConnectionString;

        public static readonly Dictionary<string,string> DicCategoryNameMap = new Dictionary<string, string>();

        static ConstantValue()
        {
            NewsDisplayAll.Add(ImportantNews);
            NewsDisplayAll.Add(XinWuDynamic);
            NewsDisplayAll.Add(CommunityDynamic);
            NewsDisplayAll.Add(VisitDynamicActivities);
            NewsDisplayAll.Add(OptimizingInformation);

            I8NewsServer = ConfigurationManager.AppSettings["I8NewsServer"].ToString();

            PrivateOfficeUrl = ConfigurationManager.AppSettings["PrivateOfficeUrl"].ToString();

            ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

            DicCategoryNameMap.Add("要闻资讯", "重要新闻");
        }
    }
}
