﻿using Biz.Util;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz
{
    public class EmployeeBiz
    {
        public string GetEmplName(string emplId)
        {
            var result = string.Empty;

            var sql = @"     SELECT 
                                    EmplName
                            FROM    [dbo].[ORG_Employee]
		                    WHERE [EmplID] = @EmplID";

            var dbResult = DapperHelper.ExcuteQueryFirstOrDefault<EmployeeDto>(sql, new { EmplID = emplId });

            if (dbResult != null)
            {
                result = dbResult.EmplName;
            }

            return result;
        }
    }
}
