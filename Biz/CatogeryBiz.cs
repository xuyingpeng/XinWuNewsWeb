﻿using Biz.Constant;
using Biz.Util;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz
{
    public class CatogeryBiz
    {
        
        /// <summary>
        /// 栏目页新闻列表检索
        /// </summary>
        /// <returns></returns>
        public List<CategoryNewsDto> SearchCatogeryNews(string keyword, string categoryGuid, string startDate, string endDate, int pageStart, int pageEnd, 
            int displayLimit, string emplId, string deptId)
        {
            var result = new List<CategoryNewsDto>();

            var sql = string.Empty;

            var roleIds = string.Empty;

            if (string.IsNullOrEmpty(emplId))
            {
                sql = @"  SELECT InfoID,
                                 Title,
                                 TitleColor,
								 Department,
								 PubInWebDate
							FROM(
									SELECT  ROW_NUMBER() OVER(ORDER BY T2.[OrderNum] ASC, T2.[PubInWebDate] DESC) AS rowid,
                                            T2.InfoID,
									        T2.Title,
									        T2.TitleColor,
											T3.DeptName AS Department,
											T2.[PubInWebDate]
									FROM    [dbo].[WebDB_Info_InCategory] T1
                                            INNER JOIN [dbo].[WebDB_Information] T2
                                            ON T1.[InfoID] = T2.[InfoID]
                                            LEFT JOIN [dbo].[ORG_Department] T3
                                            ON T2.[DeptId] = T3.[DeptId]
									WHERE T1.[CategoryGuid] = @CategoryGuid
                                    AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                                    {0}
                                    AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '')
                                        OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = ''))
                                    AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                                    AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)
								) T 
                                WHERE T.rowid > @Start
                                AND T.rowid <= @End";
            }
            else
            {
                roleIds = RoleBiz.GetRoleIdsLikeCondition(emplId);

                sql = @"  SELECT InfoID,
                                 Title,
							     TitleColor,
								 Department,
								 PubInWebDate
							FROM(
									SELECT  ROW_NUMBER() OVER(ORDER BY T2.[OrderNum] ASC,T2.[PubInWebDate] DESC) AS rowid,
                                            T2.InfoID,
									        T2.Title,
									        T2.TitleColor,
											T3.DeptName AS Department,
											T2.[PubInWebDate]
									FROM    [dbo].[WebDB_Info_InCategory] T1
                                            INNER JOIN [dbo].[WebDB_Information] T2
                                            ON T1.[InfoID] = T2.[InfoID]
                                            LEFT JOIN [dbo].[ORG_Department] T3
                                            ON T2.[DeptId] = T3.[DeptId]
									WHERE T1.[CategoryGuid] = @CategoryGuid
                                    AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                                    {0}
                                    AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '' OR T2.[ViewPermission] LIKE '%' + @EmplId + '%')
                                         OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = '' $RoleIds ))
                                    AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                                    AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)
								) T 
                                WHERE T.rowid > @Start
                                AND T.rowid <= @End";

                sql = sql.Replace("$RoleIds", roleIds);
            }

            //                                    AND T2.[EndDate] > GETDATE()

            if (!string.IsNullOrEmpty(deptId))
            {
                sql = string.Format(sql, string.Format(" AND T2.DeptId IN ({0}) ", deptId));
            }
            else
            {
                sql = string.Format(sql, string.Empty);
            }

            result = DapperHelper.ExcuteQuery<CategoryNewsDto>(sql,
                new {
                Keyword = keyword,
                CategoryGuid = categoryGuid,
                Start = pageStart,
                End = pageEnd,
                StartDate = startDate,
                EndDate = endDate,
                EmplId = emplId
                });

            foreach (var item in result)
            {
                if (item.Title != null && item.Title.Length > displayLimit)
                {
                    item.DisplayTitle = item.Title.Substring(0, displayLimit) + "...";
                }
                else
                {
                    item.DisplayTitle = item.Title;
                }

                item.DisplayDate = item.PubInWebDate.ToString("yyyy-MM-dd");
            }

            return result;
        }

        public int SearchTotalCount(string keyword, string categoryGuid, string startDate, string endDate,string emplId, string deptId)
        {
            var result = 0;

            var sql = string.Empty;

            var roleIds = string.Empty;

            if (string.IsNullOrEmpty(emplId))
            {
                sql = @"    SELECT  COUNT(1)
                            FROM    [dbo].[WebDB_Info_InCategory] T1,
                                    [dbo].[WebDB_Information] T2
		                    WHERE T1.[InfoID] = T2.[InfoID]
                            AND T1.[CategoryGuid] = @CategoryGuid
                            AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                            {0}
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '')
                                OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = ''))
                            AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                            AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)";
            }
            else
            {
                roleIds = RoleBiz.GetRoleIdsLikeCondition(emplId);

                sql = @"    SELECT  COUNT(1)
                            FROM    [dbo].[WebDB_Info_InCategory] T1,
                                    [dbo].[WebDB_Information] T2
		                    WHERE T1.[InfoID] = T2.[InfoID]
                            AND T1.[CategoryGuid] = @CategoryGuid
                            AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                            {0}
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '' OR T2.[ViewPermission] LIKE '%' + @EmplId + '%')
                                    OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = '' $RoleIds ))
                            AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                            AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)";

                sql = sql.Replace("$RoleIds", roleIds);
            }

            if (!string.IsNullOrEmpty(deptId))
            {
                sql = string.Format(sql, string.Format(" AND T2.DeptId IN ({0}) ", deptId));
            }
            else
            {
                sql = string.Format(sql, string.Empty);
            }
            
            result = DapperHelper.ExcuteCount(sql, new
            {
                Keyword = keyword,
                CategoryGuid = categoryGuid,
                StartDate = startDate,
                EndDate = endDate,
                EmplId = emplId
            });

            return result;
        }


        /// <summary>
        /// 栏目页新闻列表检索
        /// </summary>
        /// <returns></returns>
        public List<AdvancedSearchDto> AdvancedSearchCatogeryNews(string keyword, string categoryGuid, string startDate, string endDate, int pageStart, 
            int pageEnd, int displayLimit, string emplId, string deptId)
        {
            var result = new List<AdvancedSearchDto>();

            var sql = string.Empty;

            var roleIds = string.Empty;

            if (string.IsNullOrEmpty(emplId))
            {
                sql = @"  SELECT InfoID,
                                 Title,
                                 TitleColor,
								 Department,
								 PubInWebDate,
                                 CategoryGuid,
								 CategoryName
							FROM(
									SELECT  ROW_NUMBER() OVER(ORDER BY T2.[PubInWebDate] DESC) AS rowid,
                                            T2.InfoID,
									        T2.Title,
									        T2.TitleColor,
											T4.DeptName AS Department,
											T2.[PubInWebDate],
											T1.[CategoryGuid],
											T3.CategoryName
                                    FROM    [dbo].[WebDB_Info_InCategory] T1
                                            INNER JOIN [dbo].[WebDB_Information] T2
                                            ON T1.[InfoID] = T2.[InfoID]
	                                        INNER JOIN [dbo].[WebDB_Category] T3
                                            ON T1.[CategoryGuid] = T3.[CategoryGuid]
                                            LEFT JOIN [dbo].[ORG_Department] T4
                                            ON T2.[DeptId] = T4.[DeptId]
									WHERE (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                                    {0}
                                    AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '')
                                        OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = ''))
                                    AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                                    AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)
								) T 
                                WHERE T.rowid > @Start
                                AND T.rowid <= @End";
            }
            else
            {
                roleIds = RoleBiz.GetRoleIdsLikeCondition(emplId);

                sql = @"  SELECT InfoID,
                                 Title,
                                 TitleColor,
								 Department,
								 PubInWebDate,
                                 CategoryGuid,
								 CategoryName
							FROM(
									SELECT  ROW_NUMBER() OVER(ORDER BY T2.[PubInWebDate] DESC) AS rowid,
                                            T2.InfoID,
									        T2.Title,
                                            T2.TitleColor,
											T4.DeptName AS Department,
											T2.[PubInWebDate],
											T1.[CategoryGuid],
											T3.CategoryName
                                    FROM    [dbo].[WebDB_Info_InCategory] T1
                                            INNER JOIN [dbo].[WebDB_Information] T2
                                            ON T1.[InfoID] = T2.[InfoID]
	                                        INNER JOIN [dbo].[WebDB_Category] T3
                                            ON T1.[CategoryGuid] = T3.[CategoryGuid]
                                            LEFT JOIN [dbo].[ORG_Department] T4
                                            ON T2.[DeptId] = T4.[DeptId]
									WHERE (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                                    {0}
                                    AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '' OR T2.[ViewPermission] LIKE '%' + @EmplId + '%')
                                            OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = '' $RoleIds ))
                                    AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                                    AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)
								) T 
                                WHERE T.rowid > @Start
                                AND T.rowid <= @End";

                sql = sql.Replace("$RoleIds", roleIds);
            }
            //                                    AND T2.[EndDate] > GETDATE()

            var sqlCondition = string.Empty;

            if (!string.IsNullOrEmpty(deptId))
            {
                sqlCondition = string.Format(" AND T2.DeptId IN ({0}) ", deptId);

            }

            if (!string.IsNullOrEmpty(categoryGuid))
            {
                sqlCondition = sqlCondition + string.Format(" AND T1.[CategoryGuid] IN ({0}) ", categoryGuid);
            }

            sql = string.Format(sql, sqlCondition);

            result = DapperHelper.ExcuteQuery<AdvancedSearchDto>(sql,
                new
                {
                    Keyword = keyword,
                    CategoryGuid = categoryGuid,
                    Start = pageStart,
                    End = pageEnd,
                    StartDate = startDate,
                    EndDate = endDate,
                    EmplId = emplId,
                    DeptId = deptId
                });

            foreach (var item in result)
            {
                if (item.Title != null && item.Title.Length > displayLimit)
                {
                    item.Title = item.Title.Substring(0, displayLimit) + "...";
                }
                else
                {
                    item.Title = item.Title;
                }

                item.DisplayDate = item.PubInWebDate.ToString("yyyy-MM-dd");
            }

            return result;
        }

        public int AdvancedSearchTotalCount(string keyword, string categoryGuid, string startDate, string endDate, string emplId, string deptId)
        {
            var result = 0;

            var sql = string.Empty;

            var roleIds = string.Empty;

            if (string.IsNullOrEmpty(emplId))
            {
                sql = @"    SELECT  COUNT(1)
                            FROM    [dbo].[WebDB_Info_InCategory] T1,
									[dbo].[WebDB_Information] T2,
                                    [dbo].[WebDB_Category] T3
		                    WHERE T1.[InfoID] = T2.[InfoID]
                            AND T1.[CategoryGuid] = T3.[CategoryGuid]
                            {0}
                            AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '')
                                OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = ''))
                            AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                            AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)";
            }
            else
            {
                roleIds = RoleBiz.GetRoleIdsLikeCondition(emplId);

                sql = @"    SELECT  COUNT(1)
                            FROM    [dbo].[WebDB_Info_InCategory] T1,
									[dbo].[WebDB_Information] T2,
                                    [dbo].[WebDB_Category] T3
		                    WHERE T1.[InfoID] = T2.[InfoID]
                            AND T1.[CategoryGuid] = T3.[CategoryGuid]
                            {0}
                            AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '' OR T2.[ViewPermission] LIKE '%' + @EmplId + '%')
                                    OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = '' $RoleIds ))
                            AND (@StartDate = '' OR T2.[PubInWebDate] >= @StartDate)
                            AND (@EndDate = '' OR T2.[PubInWebDate] <= @EndDate)";

                sql = sql.Replace("$RoleIds", roleIds);
            }

            //                            AND T2.[EndDate] > GETDATE()

            var sqlCondition = string.Empty;

            if (!string.IsNullOrEmpty(deptId))
            {
                sqlCondition = string.Format(" AND T2.DeptId IN ({0}) ", deptId);
                
            }

            if (!string.IsNullOrEmpty(categoryGuid))
            {
                sqlCondition = sqlCondition + string.Format(" AND T1.[CategoryGuid] IN ({0}) ", categoryGuid);

            }

            sql = string.Format(sql, sqlCondition);

            result = DapperHelper.ExcuteCount(sql, new
            {
                Keyword = keyword,
                CategoryGuid = categoryGuid,
                StartDate = startDate,
                EndDate = endDate,
                EmplId = emplId,
                DeptId = deptId
            });

            return result;
        }

        public string GetCategoryOutUrl(string categoryName)
        {
            var result = string.Empty;

            var sql = @"     SELECT 
                                    [CategoryOutUrl]
                            FROM    [dbo].[WebDB_Category]
		                    WHERE [CategoryName] = @CategoryName";

            var dbResult = DapperHelper.ExcuteQueryFirstOrDefault<CategoryOutUrlDto>(sql, new { CategoryName = categoryName });

            if (dbResult != null)
            {
                result = dbResult.CategoryOutUrl;
            }

            return result;
        }

        public string GetCategoryPermission(string infoId)
        {
            var result = string.Empty;

            var sql = @"  SELECT  T2.[ViewPermission]
                            FROM    [dbo].[WebDB_Info_InCategory] T1,
                                    [dbo].[WebDB_Category] T2
		                    WHERE T1.[CategoryGuid] = T2.[CategoryGuid]
                             AND  T1.[InfoID] = @InfoID";

            var dbResult = DapperHelper.ExcuteQueryFirstOrDefault<NewsPermissionDto>(sql, new { InfoID = infoId });

            if (dbResult != null)
            {
                result = dbResult.ViewPermission;
            }

            return result;
        }

        public List<SpecialUrlDto> GetSpecialUrl(string categoryNum)
        {
            var result = new List<SpecialUrlDto>();

            var sql = @" SELECT CategoryName,CategoryGuid,CategoryOutUrl
                            FROM [dbo].[WebDB_Category]
	                        WHERE CategoryNum LIKE  ''+ @CategoryNum + '%'
	                        AND CategoryNum != @CategoryNum
                            ORDER BY OrderNum ASC ";

            result = DapperHelper.ExcuteQuery<SpecialUrlDto>(sql, new { CategoryNum = categoryNum });

            return result;
        }
    }
}
