﻿using Biz.Util;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz
{
    public class SensitivityBiz
    {
        public List<SensitivityModel> GetSensitivity()
        {
            var result = new List<SensitivityModel>();

            var sql = @" SELECT [Name]
                               ,[Replacement]
                         FROM [dbo].[WebDB_Sensitivity]";

            result = DapperHelper.ExcuteQuery<SensitivityModel>(sql, null);

            return result;
        }
    }
}
