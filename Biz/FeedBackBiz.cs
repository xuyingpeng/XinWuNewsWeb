﻿using Biz.Util;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz
{
    public class FeedBackBiz
    {
        public int DeleteFeedback(string feedbackId)
        {
            var result = 0;

            var sql = @" DELETE FROM [dbo].[WebDB_Info_FeedBack] WHERE [Row_ID]=@FeedbackId";

            result = DapperHelper.ExcuteNonQuery(sql, new { FeedbackId = feedbackId });

            return result;
        }

        public int DeleteFeedbackByEmplid(string infoId, string emplid)
        {
            var result = 0;

            var sql = @" DELETE FROM [dbo].[WebDB_Info_FeedBack] WHERE [InfoID]=@InfoID AND [OUGuid]=@OUGuid";

            result = DapperHelper.ExcuteNonQuery(sql, new { InfoID = infoId, OUGuid = emplid });

            return result;
        }

        public int SaveFeedback(string postUser, string content, string infoId, string sysGuid, string emplid)
        {
            var result = 0;

            var sql = @"INSERT INTO [dbo].[WebDB_Info_FeedBack]
                                   ([sysGuid]
                                   ,[InfoID]
                                   ,[Content]
                                   ,[PostUser]
                                   ,[PostDate]
                                   ,[OUGuid])
                            VALUES(@SysGuid
                                   ,@InfoId
                                   ,@Content
                                   ,@PostUser
                                   ,@PostDate
                                   ,@OUGuid)";

            result = DapperHelper.ExcuteNonQuery(sql,
                new { SysGuid = sysGuid, InfoId = infoId, Content = content, PostUser = postUser,
                    PostDate = DateTime.Now, OUGuid = emplid });

            return result;
        }

        public List<FeedbackDto> GetFeedback(string infoId)
        {
            var sql = @"SELECT [Row_ID] AS FeedBackId
                              ,[Content]
                              ,[PostUser]
                              ,[PostDate]
                              ,[OUGuid]
                          FROM [dbo].[WebDB_Info_FeedBack]
                          WHERE [InfoID]=@InfoID
                          ORDER BY [PostDate] DESC ";

            var result = DapperHelper.ExcuteQuery<FeedbackDto>(sql, new { InfoID = infoId });

            return result;
        }
    }
}
