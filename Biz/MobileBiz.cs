﻿using Biz.Constant;
using Biz.Util;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biz
{
    public class MobileBiz
    {
        public string GetCategoryGuid(string categoryName)
        {
            var result = string.Empty;

            var sql = @"     SELECT 
                                    [CategoryGuid] 
                            FROM    [dbo].[WebDB_Category]
		                    WHERE [CategoryName] = @CategoryName";

            var dbResult = DapperHelper.ExcuteQueryFirstOrDefault<CategoryGuidDto>(sql, new { CategoryName = categoryName });

            if (dbResult != null)
            {
                result = dbResult.CategoryGuid;
            }

            return result;
        }

        public int GetTotalCount(string categoryGuid, string emplId, string keyword)
        {
            var sql = string.Empty;

            var roleIds = string.Empty;

            if (string.IsNullOrEmpty(emplId))
            {
                sql = @"    SELECT  COUNT(1)
                            FROM    [dbo].[WebDB_Info_InCategory] T1,
                                    [dbo].[WebDB_Information] T2
		                    WHERE T1.[InfoID] = T2.[InfoID]
                            AND T1.[CategoryGuid] = @CategoryGuid
                            AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '')
                                OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = ''))";
            }
            else
            {
                roleIds = RoleBiz.GetRoleIdsLikeCondition(emplId);

                sql = @"    SELECT  COUNT(1)
                            FROM    [dbo].[WebDB_Info_InCategory] T1,
                                    [dbo].[WebDB_Information] T2
		                    WHERE T1.[InfoID] = T2.[InfoID]
                            AND T1.[CategoryGuid] = @CategoryGuid
                            AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
                            AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '' OR T2.[ViewPermission] LIKE '%' + @EmplId + '%')
                                    OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = '' $RoleIds ))";

                sql = sql.Replace("$RoleIds", roleIds);
            }

            //                            AND T2.[EndDate] > GETDATE()

            var result = DapperHelper.ExcuteCount(sql, new
            {
                CategoryGuid = categoryGuid,
                EmplId = emplId,
                Keyword = keyword
            });

            return result;
        }

        public List<MobileCategoryDto> GetCategoryList(string categoryGuid, int pageStart, int pageEnd, int displayLimit,string emplId, string keyword, string baseUrl)
        {
            var result = new List<MobileCategoryDto>();

            var sql = string.Empty;

            var roleIds = string.Empty;

            if (string.IsNullOrEmpty(emplId))
            {
                 sql = @"  SELECT InfoID,
                                 Title,
                                 TitleColor,
								 PubInWebDate
							FROM(
									SELECT  ROW_NUMBER() OVER(ORDER BY T2.[OrderNum] ASC, T2.[PubInWebDate] DESC) AS rowid,
                                            T2.InfoID,
									        T2.Title,
									        T2.TitleColor,
											T2.[PubInWebDate]
									FROM    [dbo].[WebDB_Info_InCategory] T1,
											[dbo].[WebDB_Information] T2
									WHERE T1.[InfoID] = T2.[InfoID]
									AND T1.[CategoryGuid] = @CategoryGuid
                                    AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '')
                                        OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = ''))
                                    AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
								) T 
                                WHERE T.rowid > @Start
                                AND T.rowid <= @End";
            }
            else
            {
                roleIds = RoleBiz.GetRoleIdsLikeCondition(emplId);

                sql = @"  SELECT InfoID,
                                 Title,
                                 TitleColor,
								 PubInWebDate
							FROM(
									SELECT  ROW_NUMBER() OVER(ORDER BY T2.[OrderNum] ASC, T2.[PubInWebDate] DESC) AS rowid,
                                            T2.InfoID,
									        T2.Title,
									        T2.TitleColor,
											T2.[PubInWebDate]
									FROM    [dbo].[WebDB_Info_InCategory] T1,
											[dbo].[WebDB_Information] T2
									WHERE T1.[InfoID] = T2.[InfoID]
									AND T1.[CategoryGuid] = @CategoryGuid
                                    AND (T2.[ViewChoose] = 0 AND (T2.[ViewPermission] IS NULL OR T2.[ViewPermission] = '' OR T2.[ViewPermission] LIKE '%' + @EmplId + '%')
                                            OR T2.[ViewChoose] = 1 AND (T2.[ViewRoles] IS NULL OR T2.[ViewRoles] = '' $RoleIds ))
                                    AND (@Keyword = '' OR T2.Title LIKE '%' + @Keyword + '%')
								) T 
                                WHERE T.rowid > @Start
                                AND T.rowid <= @End";

                sql = sql.Replace("$RoleIds", roleIds);
            }

            //                                    AND T2.[EndDate] > GETDATE()

            var dbResult = DapperHelper.ExcuteQuery<CategoryNewsDto>(sql, new { CategoryGuid = categoryGuid, Start = pageStart,
                End = pageEnd, EmplId = emplId,
                Keyword = keyword
            });

            foreach (var item in dbResult)
            {
                var mobileCatogeryDto = new MobileCategoryDto();

                mobileCatogeryDto.InfoID = item.InfoID;

                mobileCatogeryDto.Url = string.Format("{0}/News/MobilePage?infoId={1}", baseUrl, item.InfoID);

                if (item.Title != null && item.Title.Length > displayLimit)
                {
                    mobileCatogeryDto.Title = item.Title.Substring(0, displayLimit) + "...";
                }
                else
                {
                    mobileCatogeryDto.Title = item.Title;
                }


                if (string.IsNullOrEmpty(item.TitleColor) || item.TitleColor == "000000")
                {
                    mobileCatogeryDto.TitleColor = "#000000";
                }
                else
                {
                    mobileCatogeryDto.TitleColor = item.TitleColor;
                }

                mobileCatogeryDto.PubInWebDate = item.PubInWebDate.ToString("yyyy-MM-dd");

                result.Add(mobileCatogeryDto);
            }

            return result;
        }


        public List<AttachInfoDto> GetAttachInfo(string infoId)
        {
            var dbResult = new List<AttachInfoDto>();

            var sql = @"  SELECT [AttachId]
                                ,[FileName]
                            FROM [dbo].[WebDB_Info_Upfiles]
                            WHERE infoid = @InfoId
                            ORDER BY OrderNum";

            dbResult = DapperHelper.ExcuteQuery<AttachInfoDto>(sql, new
            {
                InfoId = infoId
            });

            return dbResult;
        }
    }
}
