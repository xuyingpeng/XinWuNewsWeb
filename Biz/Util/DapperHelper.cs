﻿using Biz.Constant;
using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Biz.Util
{
    public static class DapperHelper
    {
        static string connectionString = ConstantValue.ConnectionString;

        private const int CommandTimeout = 1800;

        /// <summary>
        /// Gets the open connection.
        /// </summary>
        /// <param name="name">The name of the connection string (optional).</param>
        /// <returns></returns>
        public static SqlConnection GetOpenConnection(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }


        public static int InsertMultiple<T>(string sql, IEnumerable<T> entities) where T : class, new()
        {
            using (SqlConnection cnn = GetOpenConnection(connectionString))
            {
                int records = 0;

                foreach (T entity in entities)
                {
                    records += cnn.Execute(sql, entity);
                }
                return records;
            }
        }

        public static DataTable ToDataTable<T>(this IList<T> list)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in list)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = props[i].GetValue(item) ?? DBNull.Value;
                table.Rows.Add(values);
            }
            return table;
        }

        public static DynamicParameters GetParametersFromObject(object obj, string[] propertyNamesToIgnore)
        {
            if (propertyNamesToIgnore == null) propertyNamesToIgnore = new string[] { String.Empty };
            DynamicParameters p = new DynamicParameters();
            PropertyInfo[] properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in properties)
            {
                if (!propertyNamesToIgnore.Contains(prop.Name))
                    p.Add("@" + prop.Name, prop.GetValue(obj, null));
            }
            return p;
        }

        public static void SetIdentity<T>(IDbConnection connection, Action<T> setId)
        {
            dynamic identity = connection.Query("SELECT @@IDENTITY AS Id").Single();
            T newId = (T)identity.Id;
            setId(newId);
        }


        public static object GetPropertyValue(object target, string propertyName)
        {
            PropertyInfo[] properties = target.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            object theValue = null;
            foreach (PropertyInfo prop in properties)
            {
                if (string.Compare(prop.Name, propertyName, true) == 0)
                {
                    theValue = prop.GetValue(target, null);
                }
            }
            return theValue;
        }

        public static void SetPropertyValue(object p, string propName, object value)
        {
            Type t = p.GetType();
            PropertyInfo info = t.GetProperty(propName);
            if (info == null)
                return;
            if (!info.CanWrite)
                return;
            info.SetValue(p, value, null);
        }

        /// <summary>
        /// Stored proc.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="procname">The procname.</param>
        /// <param name="parms">The parms.</param>
        /// <returns></returns>
        public static List<T> StoredProcWithParams<T>(string procname, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Query<T>(procname, (object)parms, commandType: CommandType.StoredProcedure).ToList();
            }

        }

        public static void StoredProcWithParams(string procname, Dictionary<string, string> parms)
        {
            var p = new DynamicParameters();
            foreach (KeyValuePair<string, string> kv in parms)
            {
                p.Add(kv.Key, kv.Value);
            }

            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                connection.Execute(procname, p, null, null, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Stored proc with params returning dynamic.
        /// </summary>
        /// <param name="procname">The procname.</param>
        /// <param name="parms">The parms.</param>
        /// <param name="connectionName">Name of the connection.</param>
        /// <returns></returns>
        public static List<dynamic> StoredProcWithParamsDynamic(string procname, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Query(procname, (object)parms, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        /// <summary>
        /// Stored proc insert with ID.
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <typeparam name="U">The Type of the ID</typeparam>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parms">instance of DynamicParameters class. This should include a defined output parameter</param>
        /// <returns>U - the @@Identity value from output parameter</returns>
        public static U StoredProcInsertWithID<T, U>(string procName, DynamicParameters parms)
        {
            using (SqlConnection connection = DapperHelper.GetOpenConnection(connectionString))
            {
                var x = connection.Execute(procName, (object)parms, commandType: CommandType.StoredProcedure);
                return parms.Get<U>("@ID");
            }
        }


        /// <summary>
        /// SQL with params.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">The SQL.</param>
        /// <param name="parms">The parms.</param>
        /// <returns></returns>
        public static List<T> ExcuteQuery<T>(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Query<T>(sql, (object)parms, null, true, CommandTimeout).ToList();
            }
        }

        public static T ExcuteQueryFirstOrDefault<T>(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.QueryFirstOrDefault<T>(sql, (object)parms, null, CommandTimeout);
            }
        }

        public static int ExcuteCount(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.QueryFirst<int>(sql, (object)parms, null, CommandTimeout);
            }
        }

        public static DataTable ExcuteQuery(string sql, dynamic parms)
        {
            var dt = new DataTable();

            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                using (var dataReader = connection.ExecuteReader(sql, (object)parms, null, CommandTimeout))
                {
                    dt.Load(dataReader);
                }
            }

            return dt;
        }

        public static int ExcuteNonQuery(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Execute(sql, (object)parms, null, CommandTimeout);
            }
        }

        /// <summary>
        /// Insert update or delete SQL.
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <param name="parms">The parms.</param>
        /// <returns></returns>
        public static int InsertUpdateOrDeleteSql(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Execute(sql, (object)parms, null, CommandTimeout);
            }
        }

        /// <summary>
        /// Insert update or delete stored proc.
        /// </summary>
        /// <param name="procName">Name of the proc.</param>
        /// <param name="parms">The parms.</param>
        /// <returns></returns>
        public static int InsertUpdateOrDeleteStoredProc(string procName, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Execute(procName, (object)parms, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// SQLs the with params single.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">The SQL.</param>
        /// <param name="parms">The parms.</param>
        /// <param name="connectionName">Name of the connection.</param>
        /// <returns></returns>
        public static T SqlWithParamsSingle<T>(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Query<T>(sql, (object)parms, null, true, CommandTimeout).FirstOrDefault();
            }
        }

        /// <summary>
        ///  proc with params single returning Dynamic object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">The SQL.</param>
        /// <param name="parms">The parms.</param>
        /// <param name="connectionName">Name of the connection.</param>
        /// <returns></returns>
        public static System.Dynamic.DynamicObject DynamicProcWithParamsSingle<T>(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Query(sql, (object)parms, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        /// <summary>
        /// proc with params returning Dynamic.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">The SQL.</param>
        /// <param name="parms">The parms.</param>
        /// <param name="connectionName">Name of the connection.</param>
        /// <returns></returns>
        public static IEnumerable<dynamic> DynamicProcWithParams<T>(string sql, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Query(sql, (object)parms, commandType: CommandType.StoredProcedure);
            }
        }


        /// <summary>
        /// Stored proc with params returning single.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="procname">The procname.</param>
        /// <param name="parms">The parms.</param>
        /// <param name="connectionName">Name of the connection.</param>
        /// <returns></returns>
        public static T StoredProcWithParamsSingle<T>(string procname, dynamic parms)
        {
            using (SqlConnection connection = GetOpenConnection(connectionString))
            {
                return connection.Query<T>(procname, (object)parms, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
        }

        #region 使用SqlBulkCopy将DataTable中的数据批量插入数据库中
        /// <summary>
        /// 使用SqlBulkCopy将DataTable中的数据批量插入数据库中
        /// </summary>
        /// <param name="strTableName">数据库中对应的表名</param>
        /// <param name="dtData">数据集</param>
        public static void SqlBulkCopyInsert(string strTableName, DataTable dtData)
        {
            if (dtData == null || dtData.Rows.Count == 0)
            {
                return;
            }

            using (SqlBulkCopy sqlRevdBulkCopy = new SqlBulkCopy(connectionString))//引用SqlBulkCopy
            {
                sqlRevdBulkCopy.BulkCopyTimeout = 0;

                sqlRevdBulkCopy.DestinationTableName = strTableName;//数据库中对应的表名

                sqlRevdBulkCopy.NotifyAfter = dtData.Rows.Count;//有几行数据

                sqlRevdBulkCopy.WriteToServer(dtData);//数据导入数据库

                sqlRevdBulkCopy.Close();//关闭连接
            }
        }
        #endregion

    }
}
