﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class CategoryGuidDto
    {
        public string CategoryGuid { get; set; }
    }
}
