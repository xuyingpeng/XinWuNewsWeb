﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AttachInfoDto
    {
        public string AttachId { get; set; }

        public string FileName { get; set; }
    }
}
