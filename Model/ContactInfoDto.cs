﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ContactInfoDto
    {
        public string Emplid { get; set; }

        public string EmplName { get; set; }

        public string Deptid { get; set; }

        public string Deptname { get; set; }

        public string Tel { get; set; }

        public string Fjh { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }
    }
}
