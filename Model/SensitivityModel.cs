﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SensitivityModel
    {
        public string Name { get; set; }

        public string Replacement { get; set; }
    }
}
