﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ApiResponseVo<T>
    {
        /// <summary>
        /// API响应结果状态
        /// </summary>
        public bool Succeed { get; set; }

        /// <summary>
        /// 结果数据条数
        /// </summary>
        public int Count { get; set; }


        /// <summary>
        /// 结果数据
        /// </summary>
        public List<T> Data { get; set; }
    }
}
