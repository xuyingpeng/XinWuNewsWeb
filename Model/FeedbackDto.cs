﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class FeedbackDto
    {
        public string FeedBackId { get; set; }

        public string Content { get; set; }

        public string PostUser { get; set; }

        public string PostDate { get; set; }

        public string OUGuid { get; set; }

        public bool isHasDeletePermission { get; set; }
    }
}
