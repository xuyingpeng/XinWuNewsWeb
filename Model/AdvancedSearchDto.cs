﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class AdvancedSearchDto
    {
        /// <summary>
        /// 新闻ID
        /// </summary>
        public string InfoID { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 标题颜色
        /// </summary>
        public string TitleColor { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PubInWebDate { get; set; }

        /// <summary>
        /// 页面显示时间
        /// </summary>
        public string DisplayDate { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 栏目Id
        /// </summary>
        public string CategoryGuid { get; set; }

        /// <summary>
        /// 栏目名称
        /// </summary>
        public string CategoryName { get; set; }
    }
}
