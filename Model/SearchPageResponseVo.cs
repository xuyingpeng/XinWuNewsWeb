﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SearchPageResponseVo<T>
    {
        /// <summary>
        /// API响应结果状态
        /// </summary>
        public bool Succeed { get; set; }

        /// <summary>
        /// 总件数(翻页用)
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TotalPage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 结果数据
        /// </summary>
        public List<T> Data { get; set; }
    }
}
