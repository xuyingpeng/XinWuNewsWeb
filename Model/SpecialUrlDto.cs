﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SpecialUrlDto
    {
        public string CategoryName { get; set; }

        public string CategoryGuid { get; set; }

        public string CategoryOutUrl { get; set; }
    }
}
