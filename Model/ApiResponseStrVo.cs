﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ApiResponseStrVo
    {
        /// <summary>
        /// API响应结果状态
        /// </summary>
        public bool Succeed { get; set; }

        /// <summary>
        /// 结果数据
        /// </summary>
        public string Data { get; set; }
    }
}
