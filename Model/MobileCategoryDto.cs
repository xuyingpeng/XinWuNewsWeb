﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class MobileCategoryDto
    {
        /// <summary>
        /// 新闻ID
        /// </summary>
        public string InfoID { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 标题颜色
        /// </summary>
        public string TitleColor { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public string PubInWebDate { get; set; }
    }
}
