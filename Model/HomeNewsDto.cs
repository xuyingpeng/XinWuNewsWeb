﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class HomeNewsDto
    {
        /// <summary>
        /// 新闻ID
        /// </summary>
        public string InfoID { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 标题颜色
        /// </summary>
        public string TitleColor { get; set; }

        /// <summary>
        /// 页面显示标题
        /// </summary>
        public string DisplayTitle { get; set; }

        /// <summary>
        /// 栏目ID
        /// </summary>
        public string CategoryGuid { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PubInWebDate { get; set; }

        /// <summary>
        /// 页面显示时间
        /// </summary>
        public string DisplayDate { get; set; }
    }
}
