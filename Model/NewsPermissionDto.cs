﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class NewsPermissionDto
    {
        public string ViewPermission { get; set; }
    }
}
